Etudiant: Arnaud Orlay
N° étu: 2170431


Principe du jeu: Tirer sur des astéroïdes.

Supports:
- PC (au moins Windows 10)
- Téléphone (au moins Android)
- Web (WebGL)


Implémentation:
- 2 scènes (le menu et le jeux)
- 3 niveaux de jeux.
- des météorites de terre (avec une robustesse différente)
- un seul vaisseau spatial
- un seul type de laser
- un bouclier
- un bonnus d'obtention du bouclier
- un bonus de gain de vie
- une compteur de vie du joueur
- un compteur du score du joueur
- une transition indiquant le niveau suivant
- l'enregistrement du meilleur score du joueur
- action de mise en pause ou arret du jeux (si jeux en cours)
- possibilité de fermer l'application depuis le menu (première scene)
- Affichage d'un joystick pour jouer sur mobile (pris sur Unity asset)
- Touches "Echap", "P" et "Pause" pour mettre en pause le jeux.


Non implémenté (par manque de temps), mais prévus:
- changer de laser
- changer de vaisseux
- ajouter des bombes
- ajouter des météorites de toute petites et des météorites de pierre (prisent dans les sprites)
- ajouter des vaisseaux enemis
- ajouter des tours de tirs énemis (sur les météorites)
- afficher un menu avec la possibilité de choisir le niveau de démarage
- attribuer un score en fonction de plusieurs critères (précision des tires, nombre de vie perdus, ...)
- ajouter un bouton "pause" pour les mobiles
- faire 9 niveaux de jeux


Problèmes rencontrés:
- pas d'intéraction avec l'interface --> refaire l'interface plusieurs fois
- design non responsive --> utiliser un canvas pour tous les composants
- difficultées de positionnement (différences entre la dimentions de la vue de la caméra et le canvas) --> utiliser les deux échelles si necessaire
- version de Unity qui bug beaucoup trop (2020.1) --> passage sur la 2020.2
- pas la possibilité de buil le projet pour Android ou WebGL --> passage sur la version 2020.2 de Unity
- c'étais les fêtes de Noël --> aucune