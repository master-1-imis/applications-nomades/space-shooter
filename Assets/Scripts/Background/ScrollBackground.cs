﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Background/Scroll")]
public class ScrollBackground : MonoBehaviour {

	public GameObject background;
	public int number;
	public float speedY;
	private ViewBorders viewBorders;
	private float sizeY;


	private void Start() {
		viewBorders = ViewBorders.Instance;
		sizeY = background.GetComponent<SpriteRenderer>().bounds.size.y;
		background.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);

		for (int i=1; i<number; i++) {
			Vector3 position = background.transform.position;
			position.y += i*sizeY;
			GameObject bgk = Instantiate(background, position, Quaternion.identity, gameObject.transform) as GameObject;
			bgk.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);
		}

	}


	private void Update() {
		// loop
		for (int i=0; i<number; i++) {
			if (gameObject.transform.GetChild(i).transform.position.y + sizeY/2 < viewBorders.borderBottom()) {
				Vector3 position = gameObject.transform.GetChild(i).transform.position;
				int pred = i-1;
				if (pred < 0) { pred += number; }
				position.y = gameObject.transform.GetChild(pred).transform.position.y + sizeY/2;
				gameObject.transform.GetChild(i).transform.position = position;
			}
		}
	}
}
