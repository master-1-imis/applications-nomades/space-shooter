﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Background/Move")]
public class MoveBackground : MonoBehaviour {

	public GameObject background1;
	public GameObject background2;
	public GameObject background3;
	public float offsetX;
	public Vector2 speed;
	private bool toRight;
	private ViewBorders viewBorders;
	private float sizeY;


	private void Start() {
		viewBorders = ViewBorders.Instance;
		sizeY = background1.GetComponent<SpriteRenderer>().bounds.size.y;
		toRight = true;
	}


	private void Update() {
		// movement
		if (toRight) {
			if (background1.transform.position.x < offsetX) {
				background1.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
				background2.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
				background3.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
			}
			else {
				toRight = false;
			}
		}
		else {
			if (background1.transform.position.x > -offsetX) {
				background1.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
				background2.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
				background3.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
			}
			else {
				toRight = true;
			}
		}

		// loop
		if (background1.transform.position.y + (sizeY/2) < viewBorders.borderBottom()) {
			Vector3 position = background1.transform.position;
			position.y = background3.transform.position.y + (sizeY/2);
			background1.transform.position = position;
		}
		if (background2.transform.position.y + (sizeY/2) < viewBorders.borderBottom()) {
			Vector3 position = background2.transform.position;
			position.y = background1.transform.position.y + (sizeY/2);
			background2.transform.position = position;
		}
		if (background3.transform.position.y + (sizeY/2) < viewBorders.borderBottom()) {
			Vector3 position = background3.transform.position;
			position.y = background2.transform.position.y + (sizeY/2);
			background3.transform.position = position;
		}
	}
}
