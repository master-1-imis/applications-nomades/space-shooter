using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSound : MonoBehaviour {

	private static ManageSound _instance;
	public AudioClip meteorImpactSound;
	public AudioClip shipImpactSoud;
	public AudioClip shootLaserSound;
	public AudioClip powerUpLifeSound;
	public AudioClip powerUpShieldUpSound;
	public AudioClip powerUpShieldDownSound;


	public static ManageSound Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<ManageSound>();
				if (_instance == null) {
					_instance = new GameObject().AddComponent<ManageSound>();
				}
			}
			return _instance;
		}
	}

	private void Awake() {
		if (_instance == null) { _instance = this; }
		else { Destroy(this); }
	}

	public void ImpactMeteor() {
		MakeSound(meteorImpactSound);
	}

	public void ImpactShip() {
		MakeSound(shipImpactSoud);
	}

	public void ShootLaser() {
		MakeSound(shootLaserSound);
	}

	public void PowerUpLife() {
		MakeSound(powerUpLifeSound);
	}

	public void PowerUpShieldUp() {
		MakeSound(powerUpShieldUpSound);
	}

	public void PowerUpShieldDown() {
		MakeSound(powerUpShieldDownSound);
	}

	private void MakeSound(AudioClip clip) {
		AudioSource.PlayClipAtPoint(clip, gameObject.transform.position);
	}
}
