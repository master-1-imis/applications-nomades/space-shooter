using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[AddComponentMenu("Scripts/Game/Manages Levels")]
public class ManageLevel : MonoBehaviour {

	public ManageGame gameManager;
	public Canvas canvas;

	[Header("Transiton between levels")]
	public GameObject transition;
	public float speedY;
	
	[Header("Levels")]
	public int startToLevel;
	public List<GameObject> levels;
	private Vector3 transitionInitialPosition;
	private int currentLevel;
	private bool canLaunchLevel;
	private bool canLaunchTransition;
	private bool isFinish;


	private void Start() {
		currentLevel = startToLevel;
		canLaunchLevel = false;
		canLaunchTransition = true;
		isFinish = false;

		// set initial position
		Vector3 position = transition.transform.localPosition;
		position.y = transition.GetComponent<RectTransform>().rect.height;
		transitionInitialPosition = position;
		transition.transform.localPosition = position;

		// deactivate all levels
		if (startToLevel < 0) { startToLevel = 0; }
		if (startToLevel >= levels.Count) { startToLevel = levels.Count - 1; }
		foreach (GameObject level in levels) {
			foreach (ManageEnemy manager in level.GetComponents<ManageEnemy>()) {
				manager.enabled = false;
			}
		}
	}


	private void Update() {
		// level 0 or screen is clear
		if (currentLevel == startToLevel || levels[currentLevel-1].transform.childCount == 0) {
			// start transition
			if (canLaunchTransition) {
				canLaunchTransition = false;
				transition.SetActive(true);
				transition.GetComponentInChildren<SetText>().Set(currentLevel);
				transition.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);
			}

			// end of transition
			else if (transition.transform.localPosition.y + transition.GetComponent<RectTransform>().rect.height/2 < canvas.GetComponent<RectTransform>().rect.yMin) {
				transition.SetActive(false);
				transition.transform.localPosition = transitionInitialPosition;
				canLaunchLevel = true;
			}

			// start current level
			if (canLaunchLevel) {
				canLaunchLevel = false;
				StartCurrentLevel();
			}

			// end of all levels (end of game)
			if (isFinish && levels[currentLevel].transform.childCount == 0) {
				gameManager.StopGameLevel();
			}
		}
	}

	private void StartCurrentLevel() {
		GameObject level = levels[currentLevel];
		Dictionary<int, List<ManageEnemy>> sections = GetSections(level);
		Dictionary<int, float> sectionsDuractions = GetDurations(sections);

		float totalDuration = 0;
		foreach (KeyValuePair<int, List<ManageEnemy>> section in sections) {
			foreach(ManageEnemy manager in section.Value) {
				StartCoroutine(EnableManager(manager, true, totalDuration + manager.startTime));	// activate manager
				StartCoroutine(EnableManager(manager, false, totalDuration + manager.endTime));		// deactivate manager
			}
			totalDuration += sectionsDuractions[section.Key];
		}

		StartCoroutine(ActiveNextLevel(totalDuration));	// activate next level
	}

	private Dictionary<int, List<ManageEnemy>> GetSections(GameObject level) {
		// return sections of levels
		HashSet<int> sectionsId = new HashSet<int>();
		Dictionary<int, List<ManageEnemy>> sections = new Dictionary<int, List<ManageEnemy>>();
		foreach(ManageEnemy manager in level.GetComponents<ManageEnemy>()) {
			if (!sectionsId.Contains(manager.sectionId)) {
				sectionsId.Add(manager.sectionId);
				List<ManageEnemy> value = new List<ManageEnemy>();
				value.Add(manager);
				sections.Add(sectionsId.Count, value);
			}
			else {
				sections[sectionsId.Count].Add(manager);
			}
		}
		foreach (int id in sections.Keys) {
			sections[id].Sort(delegate(ManageEnemy m1, ManageEnemy m2) { return m1.startTime.CompareTo(m2.startTime); });
		}
		return sections;
	}

	private Dictionary<int, float> GetDurations(Dictionary<int, List<ManageEnemy>> sections) {
		// return sections duration time in second.
		Dictionary<int, float> durations = new Dictionary<int, float>();
		foreach(KeyValuePair<int, List<ManageEnemy>> section in sections) {
			float start = section.Value.Min(manager => manager.startTime);
			float end = section.Value.Max(manager => manager.endTime);
			durations.Add(section.Key, end - start);
		}
		return durations;
	}

	private IEnumerator EnableManager(ManageEnemy manager, bool enable, float waitTime) {
		yield return new WaitForSeconds(waitTime);
		manager.enabled = enable; // activate/deactivate enemy manager
	}

	private IEnumerator ActiveNextLevel(float waitTime) {
		// next level curtain
		yield return new WaitForSeconds(waitTime);
		if (currentLevel+1 < levels.Count) {
			currentLevel++;
			canLaunchTransition = true;
		}
		else {
			isFinish = true;
		}
	}
}
