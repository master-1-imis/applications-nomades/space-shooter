﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

[AddComponentMenu("Scripts/Game/Manager Game")]
public class ManageGame : MonoBehaviour {

	private static ManageGame _instance;
	public GameObject menuPause;
	public GameObject menuEnd;
	public GameObject lifeManager;
	public TMP_Text performance;
	public bool isPlaying;
	private bool isPaused;


	public static ManageGame Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<ManageGame>();
				if (_instance == null) {
					_instance = new GameObject().AddComponent<ManageGame>();
				}
			}
			return _instance;
		}
	}

	private void Awake() {
		if (_instance == null) { _instance = this; }
		else { Destroy(this); }
	}

	private void Start() {
		isPaused = false;
		Time.timeScale = 1;
		if (performance != null) { ShowBestScore(); }
	}

	private void Update() {
		if (isPlaying) {
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Pause) || Input.GetKeyDown(KeyCode.P)) {
				if (isPaused) { ContinueGameLevel(); }
				else { PauseGameLevel(); }
			}
		}
	}

	public void ShowScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

	public void QuitGame() {
		Application.Quit();
    }

	public void PauseGameLevel() {
		if (isPlaying && !isPaused) {
			menuPause.SetActive(true);
			menuPause.GetComponent<Animator>().SetTrigger("open");
			Time.timeScale = 0;
			isPaused = true;
		}
	}

	public void ContinueGameLevel() {
		if (isPlaying && isPaused) {
			menuPause.GetComponent<Animator>().SetTrigger("close");
			menuPause.SetActive(false);
			Time.timeScale = 1;
			isPaused = false;
		}
	}

	public void StopGameLevel() {
		if (isPlaying && !isPaused) {
			menuEnd.SetActive(true);
			menuEnd.GetComponentInChildren<SetText>().Set(ManagerScore.Instance.GetScore());
			menuEnd.GetComponent<Animator>().SetTrigger("open");
			Time.timeScale = 0;
			isPaused = true;
			isPlaying = false;
		}
	}

	public void SetIsPlaying(bool playing) {
		isPlaying = playing;
	}


	public void ShowBestScore() {
		string bestScore = PlayerPrefs.GetInt("score", 0).ToString();
		performance.text = "Best score : " + bestScore;
	}

	public void SetBestScore() {
		int score = ManagerScore.Instance.GetScore();
		if (score > PlayerPrefs.GetInt("score", 0)) {
			PlayerPrefs.SetInt("score", score);
		}
	}


	// ---------- Life manager
	public void AddToLife(int toAdd) {
		// lifeManager.GetComponent<ManageLifeBar>().AddToLife(toAdd);
		if (lifeManager.TryGetComponent(out ManageLifeBar manager)) { manager.AddToLife(toAdd); }
	}

	public void RemoveToLife(int toRemove) {
		// lifeManager.GetComponent<ManageLifeBar>().RemoveToLife(toRemove);
		if (lifeManager.TryGetComponent(out ManageLifeBar manager)) { manager.RemoveToLife(toRemove); }
	}

	public int GetLife() {
		int lifePoints = 0;
		if (lifeManager.TryGetComponent(out ManageLifeBar manager)) { lifePoints = manager.GetLife(); }
		return lifePoints;
	}


	// ---------- Score manager
	public void AddToScore(int toAdd) {
		ManagerScore.Instance.AddToScore(toAdd);
		SetBestScore();
	}
}