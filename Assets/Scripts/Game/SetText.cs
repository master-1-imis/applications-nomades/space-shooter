using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetText : MonoBehaviour {

	public void Set(int score) {
		gameObject.GetComponent<TMP_Text>().text = score.ToString();
	}
}
