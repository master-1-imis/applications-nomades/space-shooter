﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Laser/Shoot Laser 1")]
public class ShootLaser1 : MonoBehaviour {

	public GameObject laser;
	public bool fromPlayer;

	private void Start() {
	}


	private void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) { Shoot(); }
	}

	public void Shoot() {
		ManageSound.Instance.ShootLaser();
		Vector3 shipSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		Vector3 shipPosition = gameObject.transform.position;
		Vector3 laserSize = laser.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().bounds.size;
		Vector3 laserPosition = new Vector3(shipPosition.x, shipPosition.y + laserSize.y/2 + shipSize.y/2 + 0.1f, shipPosition.z);
		laser.GetComponent<ManageAttributes>().belongToThePlayer = fromPlayer;
		Instantiate(laser, laserPosition, Quaternion.identity, gameObject.transform);
	}
}
