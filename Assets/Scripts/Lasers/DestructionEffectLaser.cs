﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Laser/Effect Destruction")]
public class DestructionEffectLaser : MonoBehaviour {

	private List<int> childrensIndex;
	private Color color;


	private void Start(){
		childrensIndex = new List<int>();

		for (int i=0; i<gameObject.transform.childCount; i++) {
			GameObject child = gameObject.transform.GetChild(i).gameObject;
			if (child.activeSelf) { childrensIndex.Add(i); }
		}

		if (childrensIndex.Count > 0) {
			color = gameObject.transform.GetChild(childrensIndex[0]).gameObject.GetComponent<SpriteRenderer>().color;
		}
		else {
			color = new Color(1, 1, 1, 1);
		}
	}

	private void Update() {
		if (color.a > 0) {
			color.a -= 0.03f;
			foreach (int i in childrensIndex) {
				gameObject.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>().color = color;
			}
		}
		else {
			Destroy(gameObject);
		}
	}
}