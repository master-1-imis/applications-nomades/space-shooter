﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MoveBackground::Start()
extern void MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3 (void);
// 0x00000002 System.Void MoveBackground::Update()
extern void MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E (void);
// 0x00000003 System.Void MoveBackground::.ctor()
extern void MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D (void);
// 0x00000004 System.Void ScrollBackground::Start()
extern void ScrollBackground_Start_m6C6E6A47F1F0915E5ADF9733EA33BB8CAA5D558A (void);
// 0x00000005 System.Void ScrollBackground::Update()
extern void ScrollBackground_Update_m00D44D6E0B21CF0DFAE601DC0EB6FA2B0A2EE237 (void);
// 0x00000006 System.Void ScrollBackground::.ctor()
extern void ScrollBackground__ctor_mF2E8B5FA4341B1B06A501DC440FBC8B5BAE29380 (void);
// 0x00000007 System.Void DestructionEffectEnemy::Start()
extern void DestructionEffectEnemy_Start_m2A239827B2CEC541399FB49A8FFB86846AABDFA0 (void);
// 0x00000008 System.Void DestructionEffectEnemy::Update()
extern void DestructionEffectEnemy_Update_m8ABC902696206AEBE0FC84448F6180A408BA6D54 (void);
// 0x00000009 System.Void DestructionEffectEnemy::.ctor()
extern void DestructionEffectEnemy__ctor_mE3392720B83AE27F0062F301A20B73C55DA1DD02 (void);
// 0x0000000A System.Void ManageEnemy::Start()
extern void ManageEnemy_Start_mE121432A329522875EE8D1E5DF84410B6F64FFEC (void);
// 0x0000000B System.Void ManageEnemy::Update()
extern void ManageEnemy_Update_m2C1F82864863B910C60D749433D7D2208920DE3F (void);
// 0x0000000C UnityEngine.Vector3 ManageEnemy::RandomPosition(UnityEngine.Vector3)
extern void ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D (void);
// 0x0000000D System.Void ManageEnemy::.ctor()
extern void ManageEnemy__ctor_m83E39BE85DA2DC1ACC713D9CBE2351CA24F14E2B (void);
// 0x0000000E System.Void MoveMeteor::Start()
extern void MoveMeteor_Start_mAC824C78251B2E173E463ABE962D804D035D739B (void);
// 0x0000000F System.Void MoveMeteor::Update()
extern void MoveMeteor_Update_m01742ABCA5719175AD642EB4C69A19AC2F343F24 (void);
// 0x00000010 System.Void MoveMeteor::.ctor()
extern void MoveMeteor__ctor_mAA0099A3BE88B0C4D442F25889548CB76968CC9F (void);
// 0x00000011 System.Void ManageAttributes::Start()
extern void ManageAttributes_Start_m42979D217C33A43A157787CA782057713CC25D82 (void);
// 0x00000012 System.Int32 ManageAttributes::AttackedBy(UnityEngine.GameObject)
extern void ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9 (void);
// 0x00000013 System.Int32 ManageAttributes::getLifePoints()
extern void ManageAttributes_getLifePoints_mA744A7D2223A6FD87137A28794FE7B20F4EC79E3 (void);
// 0x00000014 System.Void ManageAttributes::RestartLife()
extern void ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43 (void);
// 0x00000015 System.Void ManageAttributes::.ctor()
extern void ManageAttributes__ctor_mB772E6BCEBF8BFF7D0941864A188BFBCD3889569 (void);
// 0x00000016 System.Void ManageGame::Start()
extern void ManageGame_Start_m9216CD6AE375098C1CB437ABA04CA63CEA5EAAEE (void);
// 0x00000017 System.Void ManageGame::Update()
extern void ManageGame_Update_mA0A47EFDEDB14FEE6256A7CE372F0867E034DCEA (void);
// 0x00000018 System.Void ManageGame::ShowScene(System.String)
extern void ManageGame_ShowScene_m2D10543862AA8077F31C20FBAE91933A7920E318 (void);
// 0x00000019 System.Void ManageGame::QuitGame()
extern void ManageGame_QuitGame_mD886C1B313AC14E2D780E85A772C3438CF55D94E (void);
// 0x0000001A System.Void ManageGame::PauseGameLevel()
extern void ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4 (void);
// 0x0000001B System.Void ManageGame::ContinueGameLevel()
extern void ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99 (void);
// 0x0000001C System.Void ManageGame::.ctor()
extern void ManageGame__ctor_mA6B7A7CE4D286460F77AB226E334C14B0CC2E780 (void);
// 0x0000001D System.Void ViewBorders::Start()
extern void ViewBorders_Start_m29C7D0AB22F9C7863C2A48AA568D023FA069CC83 (void);
// 0x0000001E System.Single ViewBorders::borderTop()
extern void ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5 (void);
// 0x0000001F System.Single ViewBorders::borderBottom()
extern void ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6 (void);
// 0x00000020 System.Single ViewBorders::borderLeft()
extern void ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1 (void);
// 0x00000021 System.Single ViewBorders::borderRight()
extern void ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B (void);
// 0x00000022 System.Boolean ViewBorders::isOnBorder(UnityEngine.GameObject)
extern void ViewBorders_isOnBorder_m05F87216D6551D8CC695E4B856C9FB201D1C4E26 (void);
// 0x00000023 System.Boolean ViewBorders::isOut(UnityEngine.GameObject)
extern void ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C (void);
// 0x00000024 System.Boolean ViewBorders::isOnBorderTop(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314 (void);
// 0x00000025 System.Boolean ViewBorders::isOnBorderBottom(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB (void);
// 0x00000026 System.Boolean ViewBorders::isOnBorderLeft(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9 (void);
// 0x00000027 System.Boolean ViewBorders::isOnBorderRight(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5 (void);
// 0x00000028 System.Boolean ViewBorders::isOutFromTop(UnityEngine.GameObject)
extern void ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA (void);
// 0x00000029 System.Boolean ViewBorders::isOutFromBottom(UnityEngine.GameObject)
extern void ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622 (void);
// 0x0000002A System.Boolean ViewBorders::isOutFromLeft(UnityEngine.GameObject)
extern void ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC (void);
// 0x0000002B System.Boolean ViewBorders::isOutFromRight(UnityEngine.GameObject)
extern void ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2 (void);
// 0x0000002C System.Void ViewBorders::.ctor()
extern void ViewBorders__ctor_m7B2ADBB9D3D1CCBF60F565A10813D46D95F50561 (void);
// 0x0000002D System.Void DestructionEffectLaser::Start()
extern void DestructionEffectLaser_Start_mEE40BDCFAA8606867D4DE10BE748DBC686967E23 (void);
// 0x0000002E System.Void DestructionEffectLaser::Update()
extern void DestructionEffectLaser_Update_m8E52C4FE4399EEB7E6151DFCAB4B134BD5C268D5 (void);
// 0x0000002F System.Void DestructionEffectLaser::.ctor()
extern void DestructionEffectLaser__ctor_mF75384A5D028D953963E438F45B0C54ED26B0D1F (void);
// 0x00000030 System.Void MoveLaser1::Start()
extern void MoveLaser1_Start_m65994D75F536382A7BCEC75AD2CBBFE6A0395EFC (void);
// 0x00000031 System.Void MoveLaser1::Update()
extern void MoveLaser1_Update_mB14CB11907F598D69B541F78D97EF9613CFF234B (void);
// 0x00000032 System.Void MoveLaser1::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MoveLaser1_OnCollisionEnter2D_mE7A09A80DDF61607DA9902FCAC475D17F8805371 (void);
// 0x00000033 System.Void MoveLaser1::.ctor()
extern void MoveLaser1__ctor_m891E52378394329C7912E59536B4D5AED2E2C429 (void);
// 0x00000034 System.Void ShootLaser1::Update()
extern void ShootLaser1_Update_mA57094A89B4E1DFCCBB74B3FDD62A63F8A38AD35 (void);
// 0x00000035 System.Void ShootLaser1::.ctor()
extern void ShootLaser1__ctor_mA41F457EBC289D3DB39825F1C7ADCC529437630E (void);
// 0x00000036 System.Void MoveShip::Start()
extern void MoveShip_Start_m633249C1BDACD931611617D01EF31FE21B804819 (void);
// 0x00000037 System.Void MoveShip::Update()
extern void MoveShip_Update_mA926EB5714CE554E6AD2264E888E44D8756E30A2 (void);
// 0x00000038 UnityEngine.Vector2 MoveShip::getInput()
extern void MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749 (void);
// 0x00000039 System.Void MoveShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MoveShip_OnCollisionEnter2D_m49C54471E2C7F1AD2780BECFCD186FA8E322CFEC (void);
// 0x0000003A System.Void MoveShip::OnCollisionExit2D(UnityEngine.Collision2D)
extern void MoveShip_OnCollisionExit2D_m86C77026B656E4D502D73BC55F0E946A4F3F92EE (void);
// 0x0000003B System.Void MoveShip::.ctor()
extern void MoveShip__ctor_m6DDFD05FA781A523D7E648BAE8669F7836FD8ADA (void);
// 0x0000003C System.Void PropulsionEffect::Start()
extern void PropulsionEffect_Start_m5B5C5CF51642E66D1DA10F9A8814B042D4F73BE4 (void);
// 0x0000003D System.Void PropulsionEffect::Update()
extern void PropulsionEffect_Update_m1F2A89E9B46D24D010D6F7A150193A4304A29145 (void);
// 0x0000003E System.Void PropulsionEffect::.ctor()
extern void PropulsionEffect__ctor_mA7C23DCB8BB410B81AE781CD1A4A56161ECE8FC5 (void);
// 0x0000003F System.Void ManageLifeBar::Start()
extern void ManageLifeBar_Start_m44A0AA415C799AC8D66D3BE10A703D781C93688F (void);
// 0x00000040 System.Void ManageLifeBar::Update()
extern void ManageLifeBar_Update_mD0FBC92BCE43B59C8A5D87643662D894D00D2CEF (void);
// 0x00000041 System.Void ManageLifeBar::AddToLife(System.Int32)
extern void ManageLifeBar_AddToLife_m89DF759F658CB7AB76CD8E059C34E93D04E166E3 (void);
// 0x00000042 System.Void ManageLifeBar::RemoveToLife(System.Int32)
extern void ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D (void);
// 0x00000043 System.Int32 ManageLifeBar::GetLife()
extern void ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8 (void);
// 0x00000044 System.Void ManageLifeBar::.ctor()
extern void ManageLifeBar__ctor_m4F28E430C644E499708D8ABBF307B37230DB4B7B (void);
// 0x00000045 System.Void ManagerScore::Start()
extern void ManagerScore_Start_mDDFAC952DBBDFB4915CA9C9A97D633F2308A91E5 (void);
// 0x00000046 System.Void ManagerScore::Update()
extern void ManagerScore_Update_mB614E9A52C210E11FF01CCE6CBCED218E7C0C9C9 (void);
// 0x00000047 System.Void ManagerScore::AddToScore(System.Int32)
extern void ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775 (void);
// 0x00000048 System.Void ManagerScore::.ctor()
extern void ManagerScore__ctor_m2817EA953C04C9D3D29D0423A40954A5A410EC21 (void);
static Il2CppMethodPointer s_methodPointers[72] = 
{
	MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3,
	MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E,
	MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D,
	ScrollBackground_Start_m6C6E6A47F1F0915E5ADF9733EA33BB8CAA5D558A,
	ScrollBackground_Update_m00D44D6E0B21CF0DFAE601DC0EB6FA2B0A2EE237,
	ScrollBackground__ctor_mF2E8B5FA4341B1B06A501DC440FBC8B5BAE29380,
	DestructionEffectEnemy_Start_m2A239827B2CEC541399FB49A8FFB86846AABDFA0,
	DestructionEffectEnemy_Update_m8ABC902696206AEBE0FC84448F6180A408BA6D54,
	DestructionEffectEnemy__ctor_mE3392720B83AE27F0062F301A20B73C55DA1DD02,
	ManageEnemy_Start_mE121432A329522875EE8D1E5DF84410B6F64FFEC,
	ManageEnemy_Update_m2C1F82864863B910C60D749433D7D2208920DE3F,
	ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D,
	ManageEnemy__ctor_m83E39BE85DA2DC1ACC713D9CBE2351CA24F14E2B,
	MoveMeteor_Start_mAC824C78251B2E173E463ABE962D804D035D739B,
	MoveMeteor_Update_m01742ABCA5719175AD642EB4C69A19AC2F343F24,
	MoveMeteor__ctor_mAA0099A3BE88B0C4D442F25889548CB76968CC9F,
	ManageAttributes_Start_m42979D217C33A43A157787CA782057713CC25D82,
	ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9,
	ManageAttributes_getLifePoints_mA744A7D2223A6FD87137A28794FE7B20F4EC79E3,
	ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43,
	ManageAttributes__ctor_mB772E6BCEBF8BFF7D0941864A188BFBCD3889569,
	ManageGame_Start_m9216CD6AE375098C1CB437ABA04CA63CEA5EAAEE,
	ManageGame_Update_mA0A47EFDEDB14FEE6256A7CE372F0867E034DCEA,
	ManageGame_ShowScene_m2D10543862AA8077F31C20FBAE91933A7920E318,
	ManageGame_QuitGame_mD886C1B313AC14E2D780E85A772C3438CF55D94E,
	ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4,
	ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99,
	ManageGame__ctor_mA6B7A7CE4D286460F77AB226E334C14B0CC2E780,
	ViewBorders_Start_m29C7D0AB22F9C7863C2A48AA568D023FA069CC83,
	ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5,
	ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6,
	ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1,
	ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B,
	ViewBorders_isOnBorder_m05F87216D6551D8CC695E4B856C9FB201D1C4E26,
	ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C,
	ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314,
	ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB,
	ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9,
	ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5,
	ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA,
	ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622,
	ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC,
	ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2,
	ViewBorders__ctor_m7B2ADBB9D3D1CCBF60F565A10813D46D95F50561,
	DestructionEffectLaser_Start_mEE40BDCFAA8606867D4DE10BE748DBC686967E23,
	DestructionEffectLaser_Update_m8E52C4FE4399EEB7E6151DFCAB4B134BD5C268D5,
	DestructionEffectLaser__ctor_mF75384A5D028D953963E438F45B0C54ED26B0D1F,
	MoveLaser1_Start_m65994D75F536382A7BCEC75AD2CBBFE6A0395EFC,
	MoveLaser1_Update_mB14CB11907F598D69B541F78D97EF9613CFF234B,
	MoveLaser1_OnCollisionEnter2D_mE7A09A80DDF61607DA9902FCAC475D17F8805371,
	MoveLaser1__ctor_m891E52378394329C7912E59536B4D5AED2E2C429,
	ShootLaser1_Update_mA57094A89B4E1DFCCBB74B3FDD62A63F8A38AD35,
	ShootLaser1__ctor_mA41F457EBC289D3DB39825F1C7ADCC529437630E,
	MoveShip_Start_m633249C1BDACD931611617D01EF31FE21B804819,
	MoveShip_Update_mA926EB5714CE554E6AD2264E888E44D8756E30A2,
	MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749,
	MoveShip_OnCollisionEnter2D_m49C54471E2C7F1AD2780BECFCD186FA8E322CFEC,
	MoveShip_OnCollisionExit2D_m86C77026B656E4D502D73BC55F0E946A4F3F92EE,
	MoveShip__ctor_m6DDFD05FA781A523D7E648BAE8669F7836FD8ADA,
	PropulsionEffect_Start_m5B5C5CF51642E66D1DA10F9A8814B042D4F73BE4,
	PropulsionEffect_Update_m1F2A89E9B46D24D010D6F7A150193A4304A29145,
	PropulsionEffect__ctor_mA7C23DCB8BB410B81AE781CD1A4A56161ECE8FC5,
	ManageLifeBar_Start_m44A0AA415C799AC8D66D3BE10A703D781C93688F,
	ManageLifeBar_Update_mD0FBC92BCE43B59C8A5D87643662D894D00D2CEF,
	ManageLifeBar_AddToLife_m89DF759F658CB7AB76CD8E059C34E93D04E166E3,
	ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D,
	ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8,
	ManageLifeBar__ctor_m4F28E430C644E499708D8ABBF307B37230DB4B7B,
	ManagerScore_Start_mDDFAC952DBBDFB4915CA9C9A97D633F2308A91E5,
	ManagerScore_Update_mB614E9A52C210E11FF01CCE6CBCED218E7C0C9C9,
	ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775,
	ManagerScore__ctor_m2817EA953C04C9D3D29D0423A40954A5A410EC21,
};
static const int32_t s_InvokerIndices[72] = 
{
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1158,
	1487,
	1487,
	1487,
	1487,
	1487,
	908,
	1430,
	1487,
	1487,
	1487,
	1487,
	1237,
	1487,
	1487,
	1487,
	1487,
	1487,
	1466,
	1466,
	1466,
	1466,
	1071,
	1071,
	1071,
	1071,
	1071,
	1071,
	1071,
	1071,
	1071,
	1071,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1237,
	1487,
	1487,
	1487,
	1487,
	1487,
	1482,
	1237,
	1237,
	1487,
	1487,
	1487,
	1487,
	1487,
	1487,
	1227,
	1227,
	1430,
	1487,
	1487,
	1487,
	1227,
	1487,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	72,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
